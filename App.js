/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Button,
  Alert,
  Text,
  TextInput
} from 'react-native';

class App extends React.Component {

  state = {
    email: '',
  }

  validate = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if( reg.test(this.state.email) === false ) {
      Alert.alert("Validation Result"," \"" + this.state.email + "\"" + " is not a valid email");
    }
    else {
      Alert.alert("Validation Result"," \"" + this.state.email + "\"" + " is a valid email");
    }
  }

  render() {
    
  return (
    <>
      <View style={{backgroundColor:'grey',}}>
        <Text style={{fontSize:25,backgroundColor:'white',textAlign:'center'}}>React Native Training</Text>
        <TextInput style={{marginTop:10,color:'black',backgroundColor:'white',marginHorizontal:10,marginBottom:10,fontSize:20}} onChangeText={(email) => this.setState({email})} placeholder="Email"/>
        <View style={{marginHorizontal:100,marginVertical:10}}>
        <Button style={{backgroundColor:'#198ef9',}}
        title="Test"
        onPress={this.validate}
      />
      </View>
      </View>
    </>
  );
  }
};

export default App;
